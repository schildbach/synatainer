#!/bin/sh

# tag::doc[]
# Purges all rooms without local members from the database
#   ROOM_KEEP_LIST List of rooms to keep, even if no local members
#       starting with '/' - path to room_id list file
#       starting with '!' - room_id list
#       If 'ROOM_KEEP_LIST' is not set and a file '/conf/keep_rooms.list' exists, it is used.
#       Set 'ROOM_KEEP_LIST' to 'all' to turn this off
# end::doc[]

set -eu

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

case "${ROOM_KEEP_LIST:-yes}" in
  all|none|off|skip ) echo "Purging empty rooms turned off: (${ROOM_KEEP_LIST}). Bye."
                      exit 0
                      ;;
esac

if [ -z "${ROOM_KEEP_LIST:-}" -a -f /conf/keep_rooms.list ]; then
  ROOM_KEEP_LIST=/conf/keep_rooms.list
fi

case "${ROOM_KEEP_LIST:-all}" in
  all           ) ;;
  /*            ) if [ -f ${ROOM_KEEP_LIST} -a -r ${ROOM_KEEP_LIST} ]; then
                    tmp_keep_list=$(sort ${ROOM_KEEP_LIST})
                  else
                    echo "Error: Not a readable file: ${ROOM_KEEP_LIST}"
                    exit 1
                  fi
                  ;;
  !*            ) tmp_keep_list=$(echo ${ROOM_KEEP_LIST} | tr ' ' '\n' | sort)
                  ;;
  *             ) echo "Invalid value for ROOM_KEEP_LIST: ${ROOM_KEEP_LIST}"
                  exit 1
                  ;;
esac

if [ -n "${tmp_keep_list:-}" ]; then
  keep_list=$(mktemp)
  echo "$tmp_keep_list" >> $keep_list
  room_list=$(curl -sSL --header "Authorization: Bearer $BEARER_TOKEN" \
    "${SYNAPSE_HOST:-http://127.0.0.1:8008}/_synapse/admin/v1/rooms?limit=1000&order_by=joined_local_members&dir=b" | jq -r '.rooms[] | select(.joined_local_members == 0) | .room_id' | sort | comm -23 - $keep_list)
  rm $keep_list
else
  room_list=$(curl -sSL --header "Authorization: Bearer $BEARER_TOKEN" \
    "${SYNAPSE_HOST:-http://127.0.0.1:8008}/_synapse/admin/v1/rooms?limit=1000&order_by=joined_local_members&dir=b" | jq -r '.rooms[] | select(.joined_local_members == 0) | .room_id')
fi

echo "Purge rooms without local members."

for room_id in $room_list; do
  echo -n "Purge room $room_id -> "
  curl -sSL --header "Authorization: Bearer $BEARER_TOKEN" \
    -X DELETE -H "Content-Type: application/json" -d "{ \"purge\": true }" "${SYNAPSE_HOST:-http://127.0.0.1:8008}/_synapse/admin/v1/rooms/${room_id}"
  echo -e "\n"
done

echo "Done."
