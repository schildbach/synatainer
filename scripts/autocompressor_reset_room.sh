#!/bin/sh

# tag::doc[]
# Resets the autocompressor for a room
#
# Usage: autocompressor_reset_room.sh room_id
#
# WARNING: Mileage can vary on side effects.
#
# end::doc[]

set -eux

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE


if [ -z "${1:-}" ]; then
  echo "Usage: autocompressor_reset_room.sh room_id"
  exit 1
fi

if [ -n "${PGPASSWORD:-}" ]; then
  export PGPASSWORD
fi

psql_cmd="psql -U $DB_USER -d $DB_NAME -h $DB_HOST"

$psql_cmd -c "DELETE FROM state_compressor_state WHERE room_id='${1}';"
$psql_cmd -c "DELETE FROM state_compressor_progress WHERE room_id='${1}';"
