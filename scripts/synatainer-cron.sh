#!/bin/sh

# tag::doc[]
# Automagic synapse maintenance script.
# What it does (default):
#    daily:
#      purge all rooms without local members
#      run the state autocompressor
#    weekly:
#      delete old remote media
#      delete old message history
#    monthly:
#      vacuum the database
#
# This script is intended to be run by cron daily
# # end::doc[]

set -eu


CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

# daily 
purge_rooms_no_local_members.sh

# weekly, monday
if [ $(date '+%u') -eq 1 ]; then
  remote_cache_purge.sh
  purge_history.sh
fi

autocompressor.sh

# monthly, first
if [ $(date '+%d') -eq 1 ]; then
  case "${VACUUM_DB:-yes}" in
    none|off|skip ) echo "Vacuuming database turned off: (${VACUUM_DB})"
                    skip_vacuum=yes
                    ;;
  esac
  if [ -z "${skip_vacuum:-}" ]; then
    stui vacuum-db --full
  fi
fi
