#!/bin/sh

# tag::doc[]
# Purge old history from rooms
#   HISTORY_MAX_AGE number of days before history gets deleted, default 180
#   HISTORY_ROOM_LIST List of rooms to delete history from
#       'none'            - skip history purge
#       'all'   (default) - delete history from public joinable rooms with an alias
#       starting with '/' - path to room_id list file
#       starting with '!' - room_id list
#       If 'HISTORY_ROOM_LIST' is not set and a file '/conf/purge_history.list' exists, it is used.
#   HISTORY_KEEP_LIST List of rooms to keep the history
#       starting with '/' - path to room_id list file
#       starting with '!' - room_id list
#       If 'HISTORY_KEEP_LIST' is not set and a file '/conf/keep_history.list' exists, it is used.
# end::doc[]

set -eu

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

case "${HISTORY_ROOM_LIST:-all}" in
  none|off|skip ) echo "History purging turned off: (${HISTORY_ROOM_LIST}). Bye."
                  exit 0
                  ;;
esac

if [ -z "${HISTORY_ROOM_LIST:-}" -a -f /conf/purge_history.list ]; then
  HISTORY_ROOM_LIST=/conf/purge_history.list
fi

if [ -z "${HISTORY_KEEP_LIST:-}" -a -f /conf/keep_history.list ]; then
  HISTORY_KEEP_LIST=/conf/keep_history.list
fi

case "${HISTORY_ROOM_LIST:-all}" in
  all           ) tmp_room_list=$(curl -sSL --header "Authorization: Bearer $BEARER_TOKEN" \
                    "${SYNAPSE_HOST:-http://127.0.0.1:8008}/_synapse/admin/v1/rooms?limit=1000" | jq -r '.rooms[] | select(.federatable == true and .canonical_alias != null) | .room_id')
                  ;;
  /*            ) if [ -f ${HISTORY_ROOM_LIST} -a -r ${HISTORY_ROOM_LIST} ]; then
                    tmp_room_list=$(cat ${HISTORY_ROOM_LIST})
                  else
                    echo "Error: Not a readable file: ${HISTORY_ROOM_LIST}"
                    exit 1
                  fi
                  ;;
  !*            ) tmp_room_list=$(echo ${HISTORY_ROOM_LIST} | tr ' ' '\n')
                  ;;
  *             ) echo "Invalid value for HISTORY_ROOM_LIST: ${HISTORY_ROOM_LIST}"
                  exit 1
                  ;;
esac

before_ts=$(expr $(date '+%s') - $(expr ${HISTORY_MAX_AGE:-180} \* 86400))

if [ -n "${HISTORY_KEEP_LIST:-}" ]; then
  room_list_file=$(mktemp)
  echo "$tmp_room_list" | sort >> $room_list_file

  room_keep_file=$(mktemp)

  case "${HISTORY_KEEP_LIST}" in
      /*  ) if [ -f ${HISTORY_KEEP_LIST} -a -r ${HISTORY_KEEP_LIST} ]; then
              cat ${HISTORY_KEEP_LIST} | sort >> $room_keep_file
            else
              echo "Error: Not a readable file: ${HISTORY_KEEP_LIST}"
              exit 1
            fi
            ;;
      !*  ) echo ${HISTORY_KEEP_LIST} | tr ' ' '\n' | sort >> $room_keep_file
            ;;
      *   ) echo "Invalid value for HISTORY_ROOM_LIST: ${HISTORY_KEEP_LIST}"
            exit 1
            ;;
  esac

  room_list=$(comm -23 $room_list_file $room_keep_file)
  rm $room_list_file
  rm $room_keep_file
else
  room_list=$tmp_room_list
fi

if [ -z "$room_list" ]; then
  echo "Resulting room list was empty. Bye."
  exit 0
fi

for room_id in $room_list; do
  echo "Purge history from room: $room_id"
  purge_res=$(curl -sSL --header "Authorization: Bearer $BEARER_TOKEN" \
    -X POST -H "Content-Type: application/json" -d "{ \"purge_up_to_ts\": $before_ts }" "${SYNAPSE_HOST:-http://127.0.0.1:8008}/_synapse/admin/v1/purge_history/${room_id}")

  purge_err=$(echo "$purge_res" | jq -r '.error')

  if [ "$purge_err" != "null" ]; then
    echo "Error: $purge_err"
    continue
  fi

  purge_id=$(echo "$purge_res" | jq -r '.purge_id')

  while true ; do
    status=$(curl -sSL --header "Authorization: Bearer $BEARER_TOKEN" \
      "${SYNAPSE_HOST:-http://127.0.0.1:8008}/_synapse/admin/v1/purge_history_status/$purge_id" | jq -r '.status')

    echo "status: $status"

    if [ $status != "active" ]; then
      break
    fi

    echo "sleep 5 sec ..."
    sleep 5s

  done

  echo "Done."

done
