package main

import (
	"fmt"
	"os"
	"strings"
)

// return a string truncated to 60 runes
func sixty_runes(value string) string {
	// len is less then 60 byte, no need to do anything
	if len(value) <= 60 {
		return value
	}
	asRunes := []rune(value)
	if len(asRunes) > 60 {
		return string(asRunes[:60])
	}
	return value
}

func env_variable_info(key string, private bool) {
	val, ok := os.LookupEnv(key)
	if ok {
		if private {
			fmt.Printf("%s=%s\n", key, sixty_runes(val))
		} else {
			fmt.Printf("%s=%s\n", key, strings.Repeat("*", 15))
		}
	} else {
		fmt.Printf("%s not set\n", key)
	}
}

// test for readable file
func config_file_test(filename string) {
	fmt.Printf("Testing file '%s' .. ", filename)
	info, err := os.Stat(filename)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Println("does not exist.")
		} else {
			fmt.Printf("\nUnexpected error wile tesing: %v\n", err)
		}
		return
	}
	if info.IsDir() {
		fmt.Println("is a directory.")
		return
	}
	file, err := os.OpenFile(filename, os.O_RDONLY, 0666)
	if err != nil {
		if os.IsPermission(err) {
			fmt.Println("unsufficient permissions to read from file.")
		} else {
			fmt.Printf("\nUnexpected error wile reading: %v", err)
		}
		return
	}
	file.Close()
	fmt.Println("exists and seems readable. Nice :)")
}

func show_config_info(includePrivate bool) {
	fmt.Println("Synatainer/stui configuration info.")
	fmt.Printf("stui version: %s\n", version)
	if checkContainer() {
		fmt.Println("Running inside container. Nice.")
	} else {
		fmt.Println("No container detected.")
	}

	fmt.Println("\nEnvironment varables:")
	env_variable_info("CONFIG_FILE", true)

	env_variable_info("BEARER_TOKEN", includePrivate)
	env_variable_info("SYNAPSE_HOST", true)

	env_variable_info("DB_HOST", true)
	env_variable_info("DB_NAME", true)
	env_variable_info("DB_USER", true)
	env_variable_info("PGPASSWORD", includePrivate)

	env_variable_info("VACUUM_DB", true)

	env_variable_info("STATE_AUTOCOMPRESSOR_CHUNK_SIZE", true)
	env_variable_info("STATE_AUTOCOMPRESSOR_CHUNKS_TO_COMPRESS", true)
	env_variable_info("STATE_AUTOCOMPRESSOR_CHUNK_SIZE_BIG", true)
	env_variable_info("STATE_AUTOCOMPRESSOR_CHUNKS_TO_COMPRESS_BIG", true)

	env_variable_info("MEDIA_MAX_AGE", true)

	env_variable_info("ROOM_KEEP_LIST", true)

	env_variable_info("HISTORY_MAX_AGE", true)
	env_variable_info("HISTORY_ROOM_LIST", true)
	env_variable_info("HISTORY_KEEP_LIST", true)

	fmt.Printf("\nDefault Configuration Files:\n")
	config_file_test("/conf/synatainer.conf")
	config_file_test("/conf/keep_rooms.list")
	config_file_test("/conf/purge_history.list")
	config_file_test("/conf/keep_history.list")
	fmt.Println("Done.")
}
