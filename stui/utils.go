package main

import (
	"os"
)

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func directoryExists(dirname string) bool {
	info, err := os.Stat(dirname)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// check if we are running in a container
func checkContainer() bool {
	_, ok := os.LookupEnv("KUBERNETES_SERVICE_HOST")
	if ok {
		return true
	}
	return (fileExists("/.dockerenv") || fileExists("/run/.containerenv"))
}

func getUserConfigDir() string {
	cfgDir, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	return cfgDir
}
