module gitlab.com/mb-saces/synatainer/stui

go 1.16

require (
	github.com/integrii/flaggy v1.4.4
	github.com/jackc/pgconn v1.11.0
	github.com/jackc/pgx/v4 v4.15.0
	github.com/joho/godotenv v1.4.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/tview v0.0.0-20220307222120-9994674d60a8
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/sys v0.0.0-20220330033206-e17cdc41300f // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
)
