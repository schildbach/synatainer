package main

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/jackc/pgx/v4"
)

func db_info() {
	dburl := getPostgresConnectString()
	conn, err := pgx.Connect(context.Background(), dburl)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	var ver string
	err = conn.QueryRow(context.Background(), "SELECT version()").Scan(&ver)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Println(ver)

	// check for database superuser
	var isSuper bool
	err = conn.QueryRow(context.Background(), "SELECT usesuper FROM pg_user WHERE usename = CURRENT_USER").Scan(&isSuper)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Printf("Database superuser: %t\n", isSuper)

	// autovacuum settings
	fmt.Println("\nAutovacuum settings:")

	w := tabwriter.NewWriter(os.Stdout, 6, 8, 1, ' ', 0)

	fmt.Fprintln(w, "Option\tvalue\tunit\tdescription")

	rows, _ := conn.Query(context.Background(), "SELECT name, setting, unit, short_desc from pg_settings where category like 'Autovacuum'")

	for rows.Next() {
		var name string
		var setting string
		var unit sql.NullString
		var description string

		err := rows.Scan(&name, &setting, &unit, &description)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Scan failed: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(w, "%s\t%9s\t%s\t%s\n", name, setting, unit.String, description)
	}
	w.Flush()

	if rows.Err() != nil {
		fmt.Fprintf(os.Stderr, "Query failed: %v\n", rows.Err())
		os.Exit(1)
	}

	// table stats for vacuum
	fmt.Println("\nTable stats for vacuum:")
	w = tabwriter.NewWriter(os.Stdout, 6, 8, 1, ' ', 0)
	fmt.Fprintln(w, "table name\tdead tupels\tlast vacuum\tlast autovacuum")
	rows, _ = conn.Query(context.Background(), "SELECT relname, n_dead_tup, last_vacuum, last_autovacuum FROM pg_stat_user_tables")

	for rows.Next() {
		var name string
		var dead int
		var lvac sql.NullTime
		var lavac sql.NullTime

		err := rows.Scan(&name, &dead, &lvac, &lavac)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Scan failed: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(w, "%s\t%7d\t%s\t%s\n", name, dead, lvac.Time.String(), lavac.Time.String())
	}

	w.Flush()

	if rows.Err() != nil {
		fmt.Fprintf(os.Stderr, "Query failed: %v\n", rows.Err())
		os.Exit(1)
	}
}
